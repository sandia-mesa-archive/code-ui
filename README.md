# Sandia Mesa Code UI

Sandia Mesa Code UI is a set of customizations used to turn [Gitea](https://gitea.io) into Sandia Mesa Code.

## Copyright

Copyright (C) 2020 Sandia Mesa Animation Studios
<br>
Sandia Mesa Code UI is licensed under the [MIT License](https://code.sandiamesa.com/sandiamesa/code-ui/src/branch/master/LICENSE).